<?php
/**
 * @file 
 * Defines the page for the memory page
 */

/**
 * Render the game page
 *
 * @param int $rows
 *   The number of rows to write
 * @param int $cols
 *   The number of columns to write
 * 
 * @return string $output
 *   The content of the page
 */
function memory_game_page($rows = 2, $cols = 2) {
  $base = drupal_get_path('module', 'memory_game');
  drupal_add_js($base . '/js/memory_game.js');
  drupal_add_css($base . '/css/memory_game.css');

  $face_down_background = variable_get('memory-game-face-down-background',"#C0C0C0");
  
  //get the backgrounds to use
  $backgrounds = get_game_backgrounds($rows, $cols);
  $cell_backgrounds = array();
  for ($i = 0; $i <$rows; $i++) {
    $cell_backgrounds[$i] = array();
    for ($x = 0; $x < $cols; $x++) {
      $key = array_rand($backgrounds);
drupal_set_message($key);
      $value = $backgrounds[$key];
      unset($backgrounds[$key]);
      $cell_backgrounds[$i][$x] = array("face_down" => $face_down_background, "face_up" => $value);
    }
  }

  //write backgrounds to jQuery variables so we can grab them later
  drupal_add_js("var cellBackgrounds = " . drupal_json_encode($cell_backgrounds) . ";", 'inline');
  drupal_add_js("var cardsUp = 0;", 'inline');
  
  //build the html of the table
  $output = '';
  $output .= game_table($rows, $cols);
  
  return $output;
}

/**
 * Build table for Memory game
 *
 * @return string $table
 *   HTML of the game table
 */
function game_table($rows, $cols) {
  $table = '<table class="memory-game-table">';
  for ($i = 0; $i< $rows; $i++) { //zero index since js doesn't do associative arrays
    $table .= '<tr class="memory-game-tr">';
    for ($x = 0; $x < $cols; $x++) {
      $table .= '<td class="memory-game-td memory-game-td-face-down" id="' . $i . '-' . $x. '">&nbsp;</td>';
    }
    $table .= '</tr>';
  }
  $table .= '</table>';
  return $table;
}

/**
 * Get the full list of available backgrounds
 *
 * @todo This function should fetch the list from a database, accepting a library name or id as the parameter.
 *
 * @return array $backgrounds
 *   The complete list of backgrounds which can be selected to use
 */
function get_full_backgrounds_list() {
  $backgrounds = array(
    '#000000',
    '#FF0000',
    '#00FF00',
    '#0000FF',
    '#FFFF00',
    '#FF00FF',
  );
  return $backgrounds;
}

/**
 * get an array of backgrounds
 * 
 * @param int $rows
 *   number of rows to use
 * @param int $cols
 *   number of cols to use
 *
 * @return array $game_backgrounds
 *   Backgrounds to use
 */
function get_game_backgrounds($rows, $cols) {
  $game_backgrounds = array();
  
  $full_list = get_full_backgrounds_list();

  //figure out how many backgrounds we need
  $cells = $rows * $cols;
  $background_count = $cells/2;

  //randomly select backgrounds
  for ($i=1; $i<=$background_count; $i++) {
    $key = array_rand($full_list);
    $value = $full_list[$key];
    //if the randomly selected background is already in the array, ignore it, otherwise add it twice
    if (!in_array($full_list[$key], $game_backgrounds)) {
      for ($x=1; $x<=2; $x++) {
        $game_backgrounds[] = $value;
      }
    }
  }
  return $game_backgrounds;
}
