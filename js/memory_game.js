/**
 * @file
 * Javascript for the Memory Game module
 *
 */

var turnUpHandler = function(event){
      turnCardUp(this);
    }

(function($){
  jQuery(document).ready(function(){

    

    /*
     * Bind card turning function to onclick event
     */
    jQuery(".memory-game-td").bind("click", turnUpHandler);
  });


/**
 * Event handler for clicking a card
 *
 * @todo modify the script to use background images rather than colors
 */
function turnCardUp(clickedObject) {
    //check to ensure card is not already turned up
	var object = jQuery(clickedObject); //not sure why I have to do this
	
	if (object.hasClass("memory-game-td-face-down")) {
	  cardsUp += 1; //incrememnt the counter so we can keep track of how many cards are turned up
	  
	  //set the background for the card to the face up value
	  var coordinates = getTDCoordinates(object);
	  object.css("background-color", cellBackgrounds[coordinates[0]][coordinates[1]].face_up);
	  
	  //set class of card to face-up
	  object.removeClass("memory-game-td-face-down").addClass("memory-game-td-face-up");
	  checkCardsUp();
	}
	else {
	  alert("card is already up");
	}
}

function getTDCoordinates(object) {
  var coordinates = object.attr("id").split("-");
  return coordinates;
}

function checkCardsUp() {
  if (cardsUp == 2) {
    //disable clicking
	jQuery(".memory-game-td").unbind("click", turnUpHandler);
	
	checkMatch();
	setTimeout('resetCards()',2000);
	
	//re-enable clicking
	jQuery(".memory-game-td").bind("click", turnUpHandler);
  }
}
/**
 * check to see if two cards are a match
 */
function checkMatch() {
  var card1 = jQuery('td.memory-game-td-face-up').get(0);
  var card2 = jQuery('td.memory-game-td-face-up').get(1);
  if (jQuery(card1).css("background-color") == jQuery(card2).css("background-color")) {
    alert("You got a match!");
    jQuery(card1).toggleClass("memory-game-td-face-up").toggleClass("memory-game-td-match");
	jQuery(card2).toggleClass("memory-game-td-face-up").toggleClass("memory-game-td-match");
  }
}

/**
 * Resets cards to initial state after 2 cards have been selected
 *
 */
function resetCards() {
  cardsUp=0;  //reset counter of how many cards are turned up
  jQuery('td.memory-game-td-face-up').each(function(index) {
    var thisTD = jQuery(this);
	alert(thisTD.attr("id"));
    var coordinates = getTDCoordinates(thisTD);
 	thisTD.css("background-color", cellBackgrounds[coordinates[0]][coordinates[1]].face_down);
	thisTD.removeClass("memory-game-td-face-up").addClass("memory-game-td-face-down");
  });
}

/**
 * Resets cards to initial state after 2 cards have been selected
 *
 */
function resetGame() {
  cardsUp=0;  //reset counter of how many cards are turned up
  jQuery('td.memory-game-td').each(function(index) {
    var thisTD = jQuery(this);
    var coordinates = getTDCoordinates(thisTD);
 	thisTD.css("background-color", cellBackgrounds[coordinates[0]][coordinates[1]].face_down);
	thisTD.removeClass("memory-game-td-face-up").removeClass("memory-game-td-match").addClass("memory-game-td-face-down");
  });
}

})(jQuery);
